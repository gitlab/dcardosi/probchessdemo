package com.example.ndktest;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
//import pieces
import com.example.ndktest.Pieces;
import android.graphics.Paint.Style;
import androidx.annotation.Nullable;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Vector;

import com.example.ndktest.GameBoard;


public class ChessView extends View{
    static {
        System.loadLibrary("ndktest");
    }
    private final int boardcolor1;
    private final int boardcolor2;
    int ox;
    int oy;

    public ChessGame gameView = new ChessGame();

    public boolean movingPiece = false;
    public float movingPiecex;
    public float movingPiecey;
    public boolean aiGame=false;
    private final Paint paint = new Paint();

    private int cellSize = getWidth()/8;


    public ChessView(Context context, @Nullable AttributeSet attrs)
    {
        super(context,attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs,R.styleable.chessview,0,0);

        try{
            boardcolor1=a.getInteger(R.styleable.chessview_BoardColor1,0);
            boardcolor2=a.getInteger(R.styleable.chessview_BoardColor2,0);
            aiGame=a.getBoolean(R.styleable.chessview_AiGame,false);

        }finally{
            a.recycle();
        }

    }
    //measurement to fit any device
    @Override
    protected void onMeasure(int width, int height)
    {
        super.onMeasure(width, height);
        int dimension=Math.min(getMeasuredWidth(),getMeasuredHeight());
        cellSize = dimension/8;
        setMeasuredDimension(dimension,dimension);
    }



    protected void drawPieceAt(Canvas canvas, int row, int  col, Pieces piece)
    {
        Bitmap imageBit = BitmapFactory.decodeResource(getResources(), piece.drawableID);
        canvas.drawBitmap(imageBit,null,new Rect(cellSize * col, cellSize* row, cellSize * col + cellSize,
                cellSize* row +cellSize),paint);
    }
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        switch(e.getAction())
        {
            case MotionEvent.ACTION_DOWN:
                ox = (int)(e.getX()/cellSize);
                oy = (int)(e.getY()/cellSize);



            case MotionEvent.ACTION_UP:
                int fx = (int)(e.getX()/cellSize);
                int fy = (int)(e.getY()/cellSize);
                System.out.println(oy +" " + ox + "    " + fy + " " + fx);
                movingPiece=false;
                if(aiGame)
                {
                    if(getState(oy, ox)!=null &&(boolean)getState(oy,ox).get(1)==true)
                    {
                        verification(0,oy,ox,fy,fx);
                        verification(0,oy,ox,fy,fx);
                        invalidate();
                    }
                }else {

                    if (getState(oy, ox) != null && (boolean) getState(oy, ox).get(1) == true) {
                        verification(1, oy, ox, fy, fx);

                    }
                    invalidate();
                }
                GameBoard.updateProgressBar((int)getProbabilities().get(0),(int)getProbabilities().get(1));


        }
        return true;
    }

    public native Vector<Integer> getProbabilities();
    protected void drawAllPieces(Canvas canvas)
    {


        for(int i=0;i<8;i++)
        {
            for(int j=0;j<8;j++)
            {

                if(getState(i,j)!=null && (boolean)getState(i,j).get(1)==true&&!movingPiece)
                {
                    drawPieceAt(canvas,i,j,new Pieces(i,j,!((boolean)getState(i,j).get(2)),(int)getState(i,j).get(3),(boolean)getState(i,j).get(1)));
                }

            }
        }



        /*
    color = 0 :white
    color = 1 :black
    id = 1: pawn
    id = 2: knight
    id = 3: bishop
    id = 4: rook
    id = 5: queen
    id = 6: king
    /*

    */
    }


    @Override
    protected void onDraw(Canvas canvas)
    {
        paint.setStyle(Style.FILL);
        paint.setAntiAlias(true);
        drawGameBoard(canvas);
        drawAllPieces(canvas);


    }
    public native List<Object> getState(int rows, int cols);

    //1:boolean whose turn it is black=true; whites = false
    //2:boolean Piece at square
    //3:boolean color same as 1
    //4:int: Piece Id
    //5:vector<arrays> verified move

    public native void verification(int modetype, int row1, int col1,int row2,int col2);

    public native void startGame();

    //drawing gameboard
    private void drawGameBoard(Canvas canvas)
    {
        paint.setStrokeWidth(16);
        for(int i=0;i<8;i++)
        {
            for(int j=0;j<8;j++)
            {

                if((j+i) % 2 == 0) {
                    paint.setColor(boardcolor1);
                    canvas.drawRect(cellSize * j, cellSize* i, cellSize * j + cellSize, cellSize* i +cellSize, paint);
                } else {
                    paint.setColor(boardcolor2);
                    canvas.drawRect(cellSize * j, cellSize* i, cellSize * j + cellSize, i*cellSize+cellSize, paint);
                }
            }
        }
    }

}
